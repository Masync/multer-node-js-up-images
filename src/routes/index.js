const {Router} = require('express')
const routes = Router()
const {getData, postData} = require('../controller/routes.controller')
const multer = require('multer')
const uuid = require('uuid/v4')
const storage = multer.diskStorage({
    destination: 'src/public/uploads',
    filename: (req, file, cb)=>{
        cb(null,uuid()+path.extname(file.originalname).toLocaleLowerCase())
    }
})
const path = require('path')
const up = multer({
    storage,
    dest: 'src/public/uploads',
    fileFilter: (req, file, cb)=>{
        const filetype = /jpeg|jpg|png|gif/
        const mimetype = filetype.test(file.mimetype)
        const extname = filetype.test(path.extname(file.originalname))
        if (mimetype && extname) {
            return cb(null, true)
        }
        cb("Error: archivo no admitido")

    }
    }) 


routes.get('/', getData)
routes.post('/update',up.single('img'), postData)

module.exports = routes