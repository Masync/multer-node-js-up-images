
const app = require('./app')
require('dotenv').config({path: 'src/.env'})

async function main() {
    app.set('port', process.env.PORT)

   await app.listen(app.get('port'))
}
main()
